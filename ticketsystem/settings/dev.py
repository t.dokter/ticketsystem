# flake8: noqa
import platform

from .base import *


INSTALLED_APPS += ["debug_toolbar"]
MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]


# ==============================================================================
# EMAIL SETTINGS
# ==============================================================================

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"


# ==============================================================================
# THIRD-PARTY SETTINGS
# ==============================================================================

current_os = platform.system()

if current_os == "Windows":
    NPM_BIN_PATH = r"C:\Program Files\nodejs\npm.cmd"
