from django.db import models
from django.utils.translation import gettext_lazy as _


class Ticket(models.Model):
    PRIORITIES = (
        (0, "Laag"),
        (1, "Normaal"),
        (2, "Hoog"),
    )

    STATUSES = (
        (0, "Wachtend"),
        (1, "Geaccepteerd"),
        (2, "Afgewezen"),
        (3, "Wordt aan gewerkt"),
        (4, "Gearchiveerd"),
    )

    title = models.CharField(_("Titel"), max_length=100)
    description = models.TextField(_("Beschrijving"))
    category = models.ForeignKey(
        "tickets.Category",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="tickets",
        verbose_name="Categorie",
    )
    assignee = models.ForeignKey(
        "accounts.Account",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="tickets",
        verbose_name="Medewerker",
    )
    contact_person = models.ForeignKey(
        "contact_persons.ContactPerson",
        on_delete=models.CASCADE,
        related_name="tickets",
        verbose_name="Contact persoon",
    )
    dt_added = models.DateTimeField(_("Toevoeg datum"), auto_now_add=True)
    priority = models.IntegerField(_("Prioriteit"), choices=PRIORITIES, default=1)
    status = models.IntegerField(_("Status"), choices=STATUSES, default=0)

    def __str__(self):
        return f"{self.id:04d}"

    class Meta:
        verbose_name = "Ticket"
        verbose_name_plural = "Tickets"
