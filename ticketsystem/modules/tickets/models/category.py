from django.db import models
from django.utils.translation import gettext_lazy as _


class Category(models.Model):
    category = models.CharField(_("Categorie"), max_length=50)

    class Meta:
        verbose_name = "Categorie"
        verbose_name_plural = "Categorieën"

    def __str__(self):
        return self.category
