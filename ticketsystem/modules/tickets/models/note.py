from django.db import models
from django.utils.translation import gettext_lazy as _


class Note(models.Model):
    employee = models.ForeignKey(
        "accounts.Account",
        on_delete=models.SET_NULL,
        verbose_name="Medewerker",
        null=True,
        blank=False,
    )
    ticket = models.ForeignKey(
        "tickets.Ticket",
        on_delete=models.CASCADE,
        verbose_name="Ticket",
        related_name="notes",
    )
    note = models.TextField(_("Notitie"))
    dt_added = models.DateTimeField(_("Toevoeg datum"), auto_now_add=True)

    def __str__(self):
        return f"{self.employee.get_full_name()} - {self.ticket.title}"
