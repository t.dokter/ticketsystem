from django.views import generic
from django.db.models import Q

from ticketsystem.modules.tickets.models import Ticket


class TicketList(generic.ListView):
    template_name = "tickets/base.html"

    # Er wordt hier gefilterd op 3 van de 5 statusen. namelijk op: Wachtend, Geaccepteerd en Wordt aan gewerkt
    def get_queryset(self):
        filter = Q(Q(status=0) | Q(status=1) | Q(status=3))
        return Ticket.objects.all().filter(filter)
