from django.views import generic
from django.http import Http404

from ticketsystem.modules.tickets.models import Note
from ticketsystem.modules.tickets.forms import NoteCreateForm


# Deze view wordt gebruikt op de ticket detail pagina om notities toe te voegen
class NoteCreate(generic.CreateView):
    model = Note
    form_class = NoteCreateForm

    def get(self, request, *args, **kwargs):
        raise Http404

    def post(self, request, *args, **kwargs):
        raise ValueError(request.data)
