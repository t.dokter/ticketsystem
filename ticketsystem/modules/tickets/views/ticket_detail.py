from django.views import generic

from ticketsystem.modules.tickets.models import Ticket


class TicketDetail(generic.DetailView):
    template_name = "tickets/detail.html"

    def get_queryset(self):
        return Ticket.objects.all()
