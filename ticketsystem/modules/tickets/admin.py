from django.contrib import admin

from ticketsystem.modules.tickets.models import *


class NoteInline(admin.StackedInline):
    model = Note
    extra = 0


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("category",)


@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "category",
        "assignee",
        "contact_person",
        "priority",
        "status",
    )
    list_filter = ("priority", "status", "assignee", "category")
    inlines = [NoteInline]
