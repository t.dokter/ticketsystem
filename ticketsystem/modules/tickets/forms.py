from django import forms

from ticketsystem.modules.tickets.models import Note


class NoteCreateForm(forms.ModelForm):
    # note = forms.CharField(label="Notitie")
    class Meta:
        model = Note
        fields = ["note", "employee", "ticket"]
