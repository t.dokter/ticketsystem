from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from ticketsystem.modules.accounts.managers import AccountManager


class Account(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
        _("Email adres"),
        unique=True,
        error_messages={
            "unique": _("Er bestaat al een medewerker met dit email adres."),
        },
    )
    first_name = models.CharField(_("Voornaam"), max_length=150, blank=True)
    last_name = models.CharField(_("Achternaam"), max_length=150, blank=True)

    is_staff = models.BooleanField(
        _("Medewerker status"),
        default=False,
        help_text=_("Geeft aan of deze medewerker kan inloggen op het admin paneel."),
    )
    is_active = models.BooleanField(
        _("Actief"),
        default=True,
        help_text=_(
            "Geeft aan of deze medewerker moet worden gezien als actief. Deselecteer dit inplaats van het account verwijderen."
        ),
    )
    date_joined = models.DateTimeField(_("Toevoeg datum"), default=timezone.now)

    objects = AccountManager()

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "email"

    class Meta:
        verbose_name = _("Medewerker")
        verbose_name_plural = _("Medewerkers")

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)
