from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from ticketsystem.modules.accounts.forms import AccountCreationForm, AccountChangeForm
from ticketsystem.modules.accounts.models import Account

@admin.register(Account)
class AccountAdmin(UserAdmin):
    add_form = AccountCreationForm
    form = AccountChangeForm
    list_display = ('email', 'is_staff', 'is_active',)
    list_filter = ('is_staff', 'is_active', 'groups')
    fieldsets = (
        ('Account Details', {'fields': ('email', 'password','first_name', 'last_name')}),
        ('Permissions', {'fields': ('is_active', 'is_staff',  'groups', 'user_permissions')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active')}
        ),
    )
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)
