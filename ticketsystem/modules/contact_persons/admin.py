from django.contrib import admin

from ticketsystem.modules.contact_persons.models import ContactPerson

# Register your models here.
@admin.register(ContactPerson)
class ContactPersonAdmin(admin.ModelAdmin):
    list_display = ("email", "contact_person_name", "company", "phone_number")
