from django.apps import AppConfig


class ContactPersonsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ticketsystem.modules.contact_persons"
    verbose_name = "Contact Persons"
