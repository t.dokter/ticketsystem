from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.utils.translation import gettext_lazy as _


class ContactPerson(models.Model):
    contact_person_name = models.CharField(
        _("Naam"),
        max_length=50,
        null=True,
        blank=True,
    )
    company = models.CharField(_("Bedrijf"), max_length=255, null=True, blank=True)
    email = models.EmailField(_("Email adres"))
    phone_number = PhoneNumberField(_("Telefoon nummer"), null=True, blank=True)

    def __str__(self):
        if self.contact_person_name and self.company:
            return f"{self.contact_person_name} - {self.company}"
        elif self.contact_person_name:
            return f"{self.contact_person_name} - {self.email}"

        return self.email

    class Meta:
        verbose_name = _("Contact persoon")
        verbose_name_plural = _("Contact personen")
