# Ticketsystem

Ticket support system for MMC IT Solutions.
System build in django
For more information see fuctional design and technical design(coming soon)

## Test server links

Comming soon.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

First start with creating the right main folder structure for the project with the name of the project as your main parent folder. Then create a 'ticketsystem' folder where you can clone the project into.

Your folder should now look like this:

```
/ticketsystem <-- main parent folder
    /ticketsystem <-- project folder
```

Then in the terminal in the main parent folder use the following commands

```
cd ticketsystem/
git clone git@gitlab.com:t.dokter/ticketsystem.git .
```

You should now have the following folder structure

```
/ticketsystem
    /ticketsystem
        /media
        /requirements
        /ticketsystem
            /cdn
            /modules
            /settings
            /static
            /templates
```

After cloning the project we need to get an local database running before we can go on to the next steps of the installation which you can do with the following steps:

1. Go to your pgadmin application
2. Create a login/group with the credentials admin/admin and check the superuser privelege (skip this step if you already have this)
3. Create a local database with the name 'ticketsystem' and set the owner to the admin you just created.

## Server installation

First we need to create a virtualenvironment which we can do with de module venv. We can do this with the following commands from your main parent folder:

```
# if you only have python3.9 installed
python -m venv venv

# if you have multiple python versions
python[version] -m venv venv <-- check which extension leads to your python 3.9 version
```

After creating your venv folder, your folder structure should now look like this:

```
/ticketsystem
    /ticketsystem
        /media
        /requirements
        /ticketsystem
            /cdn
            /modules
            /settings
            /static
            /templates
    /venv
        /Include
        /Lib
        /Scripts
        /share
```

With this created we will proceed to go into our virtualenvironment and install our python packages with the following commands:

```
# for macos

source venv/bin/activate
cd ticketsystem
pip install -r requirements/dev.txt

# for windows

venv/Scripts/activate
cd ticketsystem
pip install -r requirements/dev.txt
```

Be sure you do this in your virtualenvironment.

After this you can finish your setup by creating your database and creating your superuser via the following commands:

```
# inside of your src/server folder
python manage.py migrate
python manage.py createsuperuser
```

On the new MacBooks with an M1 chip you could have the following error

```
Error loading psycopg2 module: dlopen(/Users/mmc-0002/Desktop/projecten/C20220009 - GOBO MVP/venv/lib/python3.9/site-packages/psycopg2/_psycopg.cpython-39-darwin.so, 0x0002): tried: '/Users/mmc-0002/Desktop/projecten/C20220009 - GOBO MVP/venv/lib/python3.9/site-packages/psycopg2/_psycopg.cpython-39-darwin.so' (mach-o file, but is an incompatible architecture (have 'x86_64', need 'arm64e'))
```

You can use the following commands to try to fix this:

```
brew install libpq --build-from-source
brew install openssl <-- use brew reinstall openssl@3  if you already have openssl installed

export LDFLAGS="-L/opt/homebrew/opt/openssl@1.1/lib -L/opt/homebrew/opt/libpq/lib"
export CPPFLAGS="-I/opt/homebrew/opt/openssl@1.1/include -I/opt/homebrew/opt/libpq/include"

brew services restart postgresql
```
